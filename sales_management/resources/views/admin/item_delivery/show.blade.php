@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                <h5 class="pull-left page_title">Detail Delivery Transaction</h5>
                <a class="btn btn-sm btn-primary pull-right" href="{{ route('delivery-transaction.index') }}" role="button">Back</a>
            </header>

                <table class="table table-bordered">
                    <tr>
                        <th>receipt code</th>
                        <td>{{ $deliveryTransaction->receipt_code }}</td>
                    </tr>
                    <tr>
                        <th>agent address</th>
                        <td>{{ $deliveryTransaction->agent_address }}</td>
                    </tr>
                    <tr>
                        <th>bill to</th>
                        <td>{{ $deliveryTransaction->bill_to }}</td>
                    </tr>
                    <tr>
                        <th>ship to</th>
                        <td>{{ $deliveryTransaction->ship_to }}</td>
                    </tr>
                    <tr>
                        <th>address ship</th>
                        <td>{{ $deliveryTransaction->address_ship }}</td>
                    </tr>
                    <tr>
                        <th>due date</th>
                        <td>{{ $deliveryTransaction->due_date }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                <h5 class="pull-left page_title">Item Delivery</h5>
                <a class="btn btn-sm btn-primary pull-right" href="{{ route('item-delivery.create',$deliveryTransaction->id) }}" role="button">Create New Item Delivery</a>
            </header>

                <table class="table table-bordered">
                    <tr>
                        <th>quantity</th>
                        <td>{{ $deliveryTransaction->agent_address }}</td>
                    </tr>
                    <tr>
                        <th>description</th>
                        <td>{{ $deliveryTransaction->bill_to }}</td>
                    </tr>
                    <tr>
                        <th>price</th>
                        <td>{{ $deliveryTransaction->ship_to }}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td>-</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection 