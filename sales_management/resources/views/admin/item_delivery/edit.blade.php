@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h2 class="pull-left page_title">Create New delivery transaction</h2>
        <a class="btn btn-sm btn-primary pull-right" href="{{ route('delivery-transaction.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form class="form-horizontal" action="{{ route('delivery-transaction.update',$deliveryTransaction->id) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                <div class="box-body">

                @if($errors->any())
                    <div class="form-group {{ $errors->has('receipt_code') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Receipt Code</label>
                    <div class="col-sm-10">
                        <input type="text" name="receipt_code" class="form-control" id="" placeholder="Receipt Code" value="{{ $errors->any() ? old('receipt_code') : $deliveryTransaction->receipt_code }}">
                        <span class="help-block">{{ $errors->first('receipt_code') }}</span>
                    </div>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('agent_address') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Agent Address</label>
                    <div class="col-sm-10">
                        <input type="text" name="agent_address" class="form-control" id="" placeholder="Agent Address" value="{{ $errors->any() ? old('agent_address') : $deliveryTransaction->agent_address }}">
                        <span class="help-block">{{ $errors->first('agent_address') }}</span>
                    </div>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('bill_to') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Bill To</label>
                    <div class="col-sm-10">
                        <input type="text" name="bill_to" class="form-control" id="" placeholder="Bill To" value="{{ $errors->any() ? old('bill_to') : $deliveryTransaction->bill_to }}">
                        <span class="help-block">{{ $errors->first('bill_to') }}</span>
                    </div>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('ship_to') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Ship To</label>
                    <div class="col-sm-10">
                        <input type="text" name="ship_to" class="form-control" id="" placeholder="Ship To" value="{{ $errors->any() ? old('ship_to') : $deliveryTransaction->ship_to }}">
                        <span class="help-block">{{ $errors->first('ship_to') }}</span>
                    </div>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('address_ship') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Address Ship</label>
                    <div class="col-sm-10">
                        <textarea name="address_ship" id="" cols="30" rows="5" class="form-control" placeholder="Address Ship">{{ $errors->any() ? old('address_ship') : $deliveryTransaction->address_ship }}</textarea>
                        <span class="help-block">{{ $errors->first('address_ship') }}</span>
                    </div>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('due_date') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Due Date</label>
                    <div class="col-sm-10">
                        <input type="date" name="due_date" class="form-control" id="" placeholder="Due Date" value="{{ $errors->any() ? old('due_date') : $deliveryTransaction->due_date }}">
                        <span class="help-block">{{ $errors->first('due_date') }}</span>
                    </div>
                </div>

                </div>
                <!-- /.box-body -->
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
              <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@endsection