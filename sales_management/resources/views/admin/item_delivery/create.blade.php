@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h2 class="pull-left page_title">Create New delivery transaction</h2>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form class="form-horizontal" action="{{ route('item-delivery.store',$deliveryTransactionId) }}" method="POST">
                @csrf
                <input type="hidden" name="total_input" id="total_input" value="{{ $errors->any() ? old('total_input') : 1 }}">
                <table class="table" id="item_content">
                    <tr>
                        <th>Item Name</th>
                        <th>Description</th>
                        <th width="20px">quantity</th>
                        <th>price</th>
                        <th>&nbsp;</th>
                    </tr>
                    @if($errors->any())
                        @for($i = 0; $i < old('total_input'); $i++)
                            <tr id="content_{{ $i + 1 }}">
                                <td class="{{ $errors->has('item_name.'.$i)  ? 'has-error' : 'has-success'}}">
                                    <input type="text" name="item_name[]" class="form-control" id="" placeholder="Item Name" value="{{ old('item_name.'.$i) }}">
                                    <span class="help-block">{{$errors->first('item_name.'.$i) }}</span>
                                </td>
                                 <td class="{{ $errors->has('description.'.$i)  ? 'has-error' : 'has-success'}}">
                                    <input type="text" name="description[]" class="form-control" id="" placeholder="Description" value="{{ old('description.'.$i) }}">
                                    <span class="help-block">{{$errors->first('description.'.$i) }}</span>
                                </td>
                                <td class="{{ $errors->has('qty.'.$i)  ? 'has-error' : 'has-success'}}">
                                    <input type="text" name="qty[]" class="form-control" id="" placeholder="Qty" value="{{ old('qty.'.$i) }}">
                                    <span class="help-block">{{$errors->first('qty.'.$i) }}</span>
                                </td>
                                <td class="{{ $errors->has('price.'.$i)  ? 'has-error' : 'has-success'}}">
                                    <input type="text" name="price[]" class="form-control" id="" placeholder="Price" value="{{ old('price.'.$i) }}">
                                    <span class="help-block">{{$errors->first('price.'.$i) }}</span>                                    
                                </td>
                                <td>
                                    @if($i == 0)
                                        <button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                                    @else
                                        <button class="btn btn-danger btn-remove" id="{{ $i + 1 }}" type="button"><span class="glyphicon glyphicon-minus"></span></button>
                                    @endif
                                </td>
                            </tr>
                        @endfor
                    @else
                        <tr>
                            <td><input type="text" name="item_name[]" class="form-control" id="" placeholder="Item Name" value=""></td>
                            <td><input type="text" name="description[]" class="form-control" id="" placeholder="Description" value=""></td>
                            <td><input type="text" name="qty[]" class="form-control" id="" placeholder="Qty" value=""></td>
                            <td><input type="text" name="price[]" class="form-control" id="" placeholder="Price" value=""></td>
                            <td><button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button></td>
                        </tr>
                    @endif
                </table>
              <!-- /.box-footer -->
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
              
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var a = "{{ $errors->any() ? old('total_input') : 1 }}";
            $(document).on('click','.btn-add',function(){
                a++;
                var html    = '<tr id="content_'+a+'">';
                    html   += '<td><input type="text" name="item_name[]" class="form-control" id="" placeholder="Item Name" value=""></td>';
                    html   += '<td><input type="text" name="description[]" class="form-control" id="" placeholder="Description" value=""></td>';
                    html   += '<td><input type="text" name="qty[]" class="form-control" id="" placeholder="Qty" value=""></td>';
                    html   += '<td><input type="text" name="price[]" class="form-control" id="" placeholder="Price" value=""></td>';
                    html   += '<td><button class="btn btn-danger btn-remove" type="button" id="'+a+'"><span class="glyphicon glyphicon-minus"></span></button></td>';
                    html   += '</tr>';   
                    $('#item_content').append(html);
                    $('#total_input').val(a);
            });

             $(document).on('click','.btn-remove',function(){
                var id = $(this).attr("id");
                $('#content_'+id).remove();
                a--;
                $('#total_input').val(a);
            });

        });
    </script>
@endsection