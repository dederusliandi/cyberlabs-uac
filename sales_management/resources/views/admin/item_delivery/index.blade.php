@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12 dashboard-left-cell">
            <div class="admin-content-con">
            <header class="clearfix">
                <h5 class="pull-left page_title">Delivery Transaction</h5>
                <a class="btn btn-sm btn-primary pull-right" href="{{ route('delivery-transaction.create') }}" role="button">Create new Delivery</a>
            </header>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>receipt code</th>
                        <th>agent address</th>
                        <th>bill to</th>
                        <th>ship to</th>
                        <th>due date</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                    <tr>
                        <td>no</td>
                        <td>{{ $result->receipt_code }}</td>
                        <td>{{ $result->agent_address }}</td>
                        <td>{{ $result->bill_to }}</td>
                        <td>{{ $result->ship_to }}</td>
                        <td>{{ $result->due_date }}</td>
                        <td>
                            <a class="btn btn-xs btn-warning" href="{{ route('delivery-transaction.edit',$result->id) }}" role="button">edit</a>
                            <a class="btn btn-xs btn-primary" href="{{ route('delivery-transaction.show',$result->id) }}" role="button">view</a>
                            <form action="{{ route('delivery-transaction.destroy',$result->id) }}" method="POST" style="display:inline;">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-xs btn-danger">del</button>    
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clearfix">
            <a href="#" class="pull-right text-link">view all articles</a>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection