@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
 <div id="dashboard-con">
    <div class="row">
        <div class="col-md-3">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">Agent Types</label>
                </header>
                <table>
                    <th>
                        <h1>
                            {{ $agentType }}
                        </h1>
                    </th>
                </table>
                <div class="clearfix">
                    <a href="{{ route('agent-type.index') }}" class="pull-right">
                        <button class="btn btn-info btn-sm">
                            <span class="glyphicon glyphicon-book"></span> See agent types
                        </button>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">Branches</label>
                </header>
                <table>
                    <th>
                        <h1>
                            {{ $branch }}
                        </h1>
                    </th>
                </table>
                <div class="clearfix">
                    <a href="{{ route('branch.index') }}" class="pull-right">
                        <button class="btn btn-success btn-sm">
                            <span class="glyphicon glyphicon-plane"></span> see branches
                        </button>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">Services</label>
                </header>
                <table>
                    <th>
                        <h1>
                            {{ $service }}
                        </h1>
                    </th>
                </table>
                <div class="clearfix">
                    <a href="{{ route('service.index') }}" class="pull-right">
                        <button class="btn btn-warning btn-sm">
                            <span class="glyphicon glyphicon-shopping-cart"></span> see services
                        </button>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">Delivery Transaction</label>
                </header>
                <table>
                    <th>
                        <h1>
                            {{ $deliveryTransaction }}
                        </h1>
                    </th>
                </table>
                <div class="clearfix">
                    <a href="{{ route('delivery-transaction.index') }}" class="pull-right">
                        <button class="btn btn-danger btn-sm">
                            <span class="glyphicon glyphicon-user"></span> see delivery transaction
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-md-6">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">branch data</label>
                </header>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>branch name</th>
                            <th>Telepon</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($branchData as $index =>  $branch)
                                <tr>
                                    <td>
                                        {{ $index + 1 }}
                                    </td>
                                    <td>
                                        {{ $branch->city }}
                                    </td>
                                    <td>
                                        {{ $branch->agentType->type }}
                                    </td> 
                                </tr>
                            @endforeach
                    </tbody>
                </table>
                <div class="clearfix">
                    <a href="{{ route('branch.index') }}" class="pull-right">
                        <button class="btn btn-success btn-sm">
                            <span class="glyphicon glyphicon-plane"></span> Detail
                        </button>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="dashboard-content">
                <header class="clearfix">
                    <label class="label label-default">Data User</label>
                </header>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Receive Code</th>
                            <th>Due Date</th>
                            <th>Total Item</th>
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($deliveryTransactions as $index => $deliveryTransaction)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $deliveryTransaction->receipt_code }}</td>
                                <td>{{ $deliveryTransaction->due_date }}</td>
                                <td>
                                <label class="label label-warning">{{ $deliveryTransaction->itemDelivery->count() }}</label> 
                                </td>
                            </tr>
                       @endforeach
                        
                    </tbody>
                </table>
                <div class="clearfix">
                    <a href="{{ route('delivery-transaction.index') }}" class="pull-right">
                        <button class="btn btn-primary btn-sm">
                            <span class="glyphicon glyphicon-user"></span> detail
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

   


@endsection