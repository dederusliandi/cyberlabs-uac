<div class="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">
    <br>
    <h1 class="hidden-xs hidden-sm">Sales</h1>
    <br>
    <ul>
        
        <li class="link {{ $page == 'dashboard' ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}">
                <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Dashboard</span>
            </a>
        </li>

        <li class="link {{ $page == 'agent_type' ? 'active' : '' }}">
            <a href="{{ route('agent-type.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Agent Types</span>
            </a>
        </li>

         <li class="link {{ $page == 'branch' ? 'active' : '' }}">
            <a href="{{ route('branch.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Branches</span>
            </a>
        </li>

        <li class="link {{ $page == 'service' ? 'active' : '' }}">
            <a href="{{ route('service.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Services</span>
            </a>
        </li>

        <li class="link {{ $page == 'delivery_transaction' ? 'active' : '' }}">
            <a href="{{ route('delivery-transaction.index') }}">
                <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Delivery Transaction</span>
            </a>
        </li>
    </ul>
</div>