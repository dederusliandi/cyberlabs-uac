@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12 dashboard-left-cell">
            <div class="admin-content-con">
                <header class="clearfix">
                    <h5 class="pull-left page_title">Data Branches</h5>
                    <a class="btn btn-sm btn-primary pull-right" href="{{ route('branch.create') }}" role="button">Create new Branch</a>
                </header>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>agent type</th>
                            <th>city</th>
                            <th>address</th>
                            <th>phone</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{ $result->agentType->type }}</td>
                            <td>{{ $result->city }}</td>
                            <td>{{ $result->address }}</td>
                            <td>{{ $result->phone }}</td>
                            <td>
                                <a class="btn btn-xs btn-warning" href="{{ route('branch.edit',$result->id) }}" role="button">edit</a>
                                <a class="btn btn-xs btn-primary" href="{{ route('branch.show',$result->id) }}" role="button">view</a>
                                <form action="{{ route('branch.destroy',$result->id) }}" method="POST" style="display:inline-block;">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-xs btn-danger">del</button>    
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <nav>
                    {{ $results->links() }}
                </nav>
                
                <div class="clearfix">
                <a href="#" class="pull-right text-link">view all articles</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection