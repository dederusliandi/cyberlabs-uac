@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h2 class="pull-left page_title">Create New Branch</h2>
        <a class="btn btn-sm btn-primary pull-right" href="{{ route('branch.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form class="form-horizontal" action="{{ route('branch.store') }}" method="POST">
                @csrf
                <div class="box-body">

                @if($errors->any())
                    <div class="form-group {{ $errors->has('agent_type_id') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Agent type</label>
                    <div class="col-sm-10">
                        <select name="agent_type_id" id="" class="form-control">
                            <option value="">Choose Agent Type</option>
                            @foreach($agentTypes as $agentType)
                                <option value="{{ $agentType->id }}" {{ old('agent_type_id') == $agentType->id ? 'selected' : '' }}>{{ $agentType->type }}</option>
                            @endforeach
                        </select>
                        <span class="help-block">{{ $errors->first('agent_type_id') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('city') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" name="city" class="form-control" id="" placeholder="City" value="{{ old('city') }}">
                        <span class="help-block">{{ $errors->first('city') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <textarea name="address" id="" cols="30" rows="5" class="form-control" placeholder="Address">{{ old('address') }}</textarea>
                        <span class="help-block">{{ $errors->first('address') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('code') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-10">
                        <input type="text" name="code" class="form-control" id="" placeholder="Code" value="{{ old('code') }}">
                        <span class="help-block">{{ $errors->first('code') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" id="" placeholder="Phone" value="{{ old('phone') }}">
                        <span class="help-block">{{ $errors->first('phone') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('fax') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Fax</label>
                    <div class="col-sm-10">
                        <input type="text" name="fax" class="form-control" id="" placeholder="Fax" value="{{ old('fax') }}">
                        <span class="help-block">{{ $errors->first('fax') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('contact') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Contact</label>
                    <div class="col-sm-10">
                        <input type="text" name="contact" class="form-control" id="" placeholder="Contact" value="{{ old('contact') }}">
                        <span class="help-block">{{ $errors->first('contact') }}</span>
                    </div>
                </div>

                 @if($errors->any())
                    <div class="form-group {{ $errors->has('cellular') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label for="" class="col-sm-2 control-label">Cellular</label>
                    <div class="col-sm-10">
                        <input type="text" name="cellular" class="form-control" id="" placeholder="Cellular" value="{{ old('cellular') }}">
                        <span class="help-block">{{ $errors->first('cellular') }}</span>
                    </div>
                </div>

                </div>
                <!-- /.box-body -->
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
              <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@endsection