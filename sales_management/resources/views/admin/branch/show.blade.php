@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                <h5 class="pull-left page_title">Detail Branch</h5>
                <a class="btn btn-sm btn-primary pull-right" href="{{ route('branch.index') }}" role="button">Back</a>
            </header>

                <table class="table table-bordered">
                    <tr>
                        <th>agent type</th>
                        <td>{{ $branch->agentType->type }}</td>
                    </tr>
                    <tr>
                        <th>city</th>
                        <td>{{ $branch->city }}</td>
                    </tr>
                    <tr>
                        <th>address</th>
                        <td>{{ $branch->address }}</td>
                    </tr>
                    <tr>
                        <th>code</th>
                        <td>{{ $branch->code }}</td>
                    </tr>
                    <tr>
                        <th>phone</th>
                        <td>{{ $branch->phone }}</td>
                    </tr>
                    <tr>
                        <th>fax</th>
                        <td>{{ $branch->fax }}</td>
                    </tr>
                    <tr>
                        <th>contact</th>
                        <td>{{ $branch->contact }}</td>
                    </tr>
                    <tr>
                        <th>cellular</th>
                        <td>{{ $branch->cellular }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection 