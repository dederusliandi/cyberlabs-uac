@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                    <h5 class="pull-left page_title">Services</h5>
                    <a class="btn btn-sm btn-primary pull-right" href="{{ route('service.create') }}" role="button">Create new Service</a>
                </header>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Service Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($results as $key => $result)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $result->name }}</td>
                                <td>
                                <a href="{{ route('service.edit',$result->id) }}" class="btn btn-warning btn-xs">edit</a>
                                <form action="{{ route('service.destroy',$result->id) }}" method="POST" style="display: inline-block;">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-xs">delete</button>
                                </form>    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection