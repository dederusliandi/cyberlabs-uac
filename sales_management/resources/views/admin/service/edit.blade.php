@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h5 class="pull-left page_title">Edit agent types</h5>
        <a class="btn btn-sm btn-primary pull-right" href="{{ route('service.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('service.update',$service->id) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                @if($errors->any())
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Nama tipe agen</label>
                    <input type="text" name="name" class="form-control" placeholder="service name" value="{{ $errors->any() ? old('type') : $service->name }}">
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>
                
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection