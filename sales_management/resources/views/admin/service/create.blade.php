@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h5 class="pull-left page_title">Create new Services</h5>
        <a class="btn btn-sm btn-primary pull-right" href="{{ route('service.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('service.store') }}" method="POST">
                @csrf
                @if($errors->any())
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Service name</label>
                    <input type="text" name="name" class="form-control" placeholder="service name" value="{{ old('name') }}">
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection