@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                    <h5 class="pull-left page_title">Agent types</h5>
                    <a class="btn btn-sm btn-primary pull-right" href="{{ route('agent-type.create') }}" role="button">Create new Agent Type</a>
                </header>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Agent Type Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($results as $key => $result)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $result->type }}</td>
                                <td>
                                <a href="{{ route('agent-type.edit',$result->id) }}" class="btn btn-warning btn-xs">edit</a>
                                <form action="{{ route('agent-type.destroy',$result->id) }}" method="POST" style="display:inline-block;">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger btn-xs">delete</button>
                                </form>    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection