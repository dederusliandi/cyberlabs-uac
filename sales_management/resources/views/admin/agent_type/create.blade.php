@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h5 class="pull-left page_title">Create new agent types</h5>
        <a class="btn btn-sm btn-primary pull-right" href="{{ route('agent-type.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('agent-type.store') }}" method="POST">
                @csrf
                @if($errors->any())
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Nama tipe agen</label>
                    <input type="text" name="type" class="form-control" placeholder="nama type agen" value="{{ old('type') }}">
                    <span>{{ $errors->first('type') }}</span>
                </div>
                

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection