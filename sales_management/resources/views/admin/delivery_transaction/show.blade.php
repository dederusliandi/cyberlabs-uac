@extends('admin.layouts.app')

@section('head')
<link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header class="clearfix">
                <h5 class="pull-left page_title">Detail Delivery Transaction</h5>
                <a class="btn btn-sm btn-primary pull-right" href="{{ route('delivery-transaction.index') }}" role="button">Back</a>
            </header>

                <table class="table table-bordered">
                    <tr>
                        <th>Receipt code</th>
                        <td>{{ $deliveryTransaction->receipt_code }}</td>
                    </tr>
                    <tr>
                        <th>Agent address</th>
                        <td>{{ $deliveryTransaction->agent_address }}</td>
                    </tr>
                    <tr>
                        <th>Bill to</th>
                        <td>{{ $deliveryTransaction->bill_to }}</td>
                    </tr>
                    <tr>
                        <th>Ship to</th>
                        <td>{{ $deliveryTransaction->ship_to }}</td>
                    </tr>
                    <tr>
                        <th>Service</th>
                        <td>{{ $deliveryTransaction->service->name }}</td>
                    </tr>
                    <tr>
                        <th>Address ship</th>
                        <td>{{ $deliveryTransaction->address_ship }}</td>
                    </tr>
                    <tr>
                        <th>Due date</th>
                        <td>{{ $deliveryTransaction->due_date }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
            <header class="clearfix">
                <h5 class="pull-left page_title">Item Delivery</h5>
                {{-- <a class="btn btn-sm btn-primary pull-right" href="{{ route('item-delivery.create',$deliveryTransaction->id) }}" role="button">Create New Item Delivery</a> --}}
            </header>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>item name</th>
                            <th>quantity</th>
                            <th>price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($deliveryTransaction->itemDelivery as $item)
                            @php
                                $total += $item->qty * $item->price;
                            @endphp
                            <tr>
                                <td>{{ $item->item_name }}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ number_format($item->price,2) }}</td>
                                <td>{{ number_format($item->qty * $item->price,2) }}</td>
                            </tr>
                        @endforeach
                            <tr>
                                <td>total</td>
                                <td></td>
                                <td></td>
                                <td>{{ number_format($total,2) }}</td>
                            </tr>
                    </body>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection 