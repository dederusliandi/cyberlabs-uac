<?php

use Illuminate\Database\Seeder;

class AgentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\AgentType::insert([
            [
                'type'  => 'kantor perwakilan',
            ],
            [
                'type'  => 'agen utama',
            ],
            [
                'type'  => 'sub agen'
            ],
        ]);
    }
}
