<?php

use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 2000; $i++) {
            App\Model\Branch::create([
                'agent_type_id' => rand(1,3),
                'city'          => $faker->city,
                'address'       => $faker->address,
                'code'          => $faker->buildingNumber,
                'phone'         => $faker->phoneNumber,
                'fax'           => $faker->phoneNumber,
                'contact'       => $faker->firstName,
                'cellular'      => $faker->phoneNumber,
            ]);
        }
    }
}
