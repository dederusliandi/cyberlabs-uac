<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Service::insert([
            [
                'name'  => 'express'
            ],
            [
                'name'  => 'regular'
            ],
            [
                'name'  => 'special'
            ],
        ]);
    }
}
