<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_transaction_id')->unsigned();
            $table->string('item_name');
            $table->integer('qty');
            $table->bigInteger('price');
            $table->text('description');
            $table->timestamps();

            $table->foreign('delivery_transaction_id')
                  ->references('id')
                  ->on('delivery_transactions')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_deliveries');
    }
}
