<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Request;

class ActiveLinkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $page = '';

        if(Request::segment(1) == '')
        {
            $page = 'dashboard';
        }

        if(Request::segment(1) == 'agent-type')
        {
            $page = 'agent_type';
        }

        if(Request::segment(1) == 'branch')
        {
            $page = 'branch';
        }

        if(Request::segment(1) == 'service')
        {
            $page = 'service';
        }

        if(Request::segment(1) == 'delivery-transaction')
        {
            $page = 'delivery_transaction';
        }

        view()->share('page',$page);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
