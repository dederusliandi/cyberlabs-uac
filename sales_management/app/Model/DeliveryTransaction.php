<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeliveryTransaction extends Model
{
    protected $fillable = [
        'service_id',
        'receipt_code',
        'agent_address',
        'bill_to',
        'ship_to',
        'address_ship',
        'due_date'
    ];

    function itemDelivery()
    {
        return $this->hasMany('App\Model\ItemDelivery');
    }

    function service()
    {
        return $this->belongsTo('App\Model\Service');
    }
}
