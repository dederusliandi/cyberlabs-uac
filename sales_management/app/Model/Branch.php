<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'agent_type_id',
        'city',
        'address',
        'code',
        'phone',
        'fax',
        'contact',
        'cellular',
    ];

    function agentType()
    {
        return $this->belongsTo('App\Model\AgentType');
    }
}
