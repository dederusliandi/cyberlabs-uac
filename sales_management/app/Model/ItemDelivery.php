<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemDelivery extends Model
{
    protected $fillable = [
        'delivery_transaction_id',
        'item_name',
        'qty',
        'price',
        'description'
    ];

    function deliveryTransaction()
    {
        return $this->belongsTo('App\Model\DeliveryTransaction');
    }
}
