<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AgentType extends Model
{
    protected $fillable = ['type'];

    function branch()
    {
        return $this->hasMany('App\Model\Branch');
    }
}
