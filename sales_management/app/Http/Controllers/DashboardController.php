<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function index()
    {
        $agentType              =  \App\Model\AgentType::count();
        $branch                 =  \App\Model\Branch::count();
        $service                =  \App\Model\Service::count();
        $deliveryTransaction    =  \App\Model\DeliveryTransaction::count();
        $branchData             =  \App\Model\Branch::orderBy('id','DESC')->limit(5)->get();
        $deliveryTransactions   =  \App\Model\DeliveryTransaction::orderBy('id','DESC')->limit(5)->get();
        return view('admin.dashboard.index',compact('agentType','branch','service','deliveryTransaction','branchData','deliveryTransactions'));
    }
}
