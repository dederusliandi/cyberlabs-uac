<?php

namespace App\Http\Controllers;

use App\Model\DeliveryTransaction;
use App\Model\Service;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryTransactionRequest;

class DeliveryTransactionController extends Controller
{

    public function index()
    {
        $results = DeliveryTransaction::all();
        return view('admin.delivery_transaction.index',compact('results'));
    }

    public function create()
    {
        $services = Service::all();
        return view('admin.delivery_transaction.create',compact('services'));
    }

    public function store(DeliveryTransactionRequest $request)
    {
        $data = DeliveryTransaction::create($request->all());
        return redirect()->route('item-delivery.create',$data->id);
    }

    public function show(DeliveryTransaction $deliveryTransaction)
    {
        return view('admin.delivery_transaction.show',compact('deliveryTransaction'));
    }

    public function edit(DeliveryTransaction $deliveryTransaction)
    {
        return view('admin.delivery_transaction.edit',compact('deliveryTransaction'));
    }

    public function update(DeliveryTransactionRequest $request, DeliveryTransaction $deliveryTransaction)
    {
        $deliveryTransaction->update($request->all());
        return redirect()->route('delivery-transaction.index');
    }

    public function destroy(DeliveryTransaction $deliveryTransaction)
    {
        $deliveryTransaction->delete();
        return redirect()->route('delivery-transaction.index');
    }
}
