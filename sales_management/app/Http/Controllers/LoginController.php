<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

class LoginController extends Controller
{
    function callback(Request $request)
    {
        Session::put('token',$request->token);
        return redirect()->intended($request->page);
    }

    function logout()
    {
        Session::flush();
    }
}
