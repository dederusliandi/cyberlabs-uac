<?php

namespace App\Http\Controllers;

use App\Model\ItemDelivery;
use Illuminate\Http\Request;
use App\Http\Requests\ItemDeliveryRequest;

class ItemDeliveryController extends Controller
{

    public function create($deliveryTransactionId)
    {
        return view('admin.item_delivery.create',compact('deliveryTransactionId'));
    }

    public function store(ItemDeliveryRequest $request,$deliveryTransactionId)
    {
        for($i = 0; $i < count($request->item_name); $i++)
        {
            ItemDelivery::create([
                'delivery_transaction_id'   => $deliveryTransactionId,
                'item_name'                 => $request->item_name[$i],
                'qty'                       => $request->qty[$i],
                'price'                     => $request->price[$i],
                'description'               => $request->description[$i],
            ]);
        }

        return redirect()->route('delivery-transaction.show',$deliveryTransactionId);
    }
}
