<?php

namespace App\Http\Controllers;

use App\Model\AgentType;
use Illuminate\Http\Request;
use App\Http\Requests\AgentTypeRequest;

class AgentTypeController extends Controller
{
    public function index()
    {
        $results = AgentType::all();
        return view('admin.agent_type.index',compact('results'));
    }

    public function create()
    {
        return view('admin.agent_type.create');
    }

    public function store(AgentTypeRequest $request)
    {
        AgentType::create($request->all());
        return redirect()->route('agent-type.index');
    }

    public function edit(AgentType $agentType)
    {
        return view('admin.agent_type.edit',compact('agentType'));
    }

    public function update(Request $request, AgentType $agentType)
    {
        $agentType->update($request->all());
        return redirect()->route('agent-type.index');
    }

    public function destroy(AgentType $agentType)
    {
        $agentType->delete();
        return redirect()->route('agent-type.index');
    }
}
