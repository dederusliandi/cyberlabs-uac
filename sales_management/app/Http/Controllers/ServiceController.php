<?php

namespace App\Http\Controllers;

use App\Model\Service;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;

class ServiceController extends Controller
{
    public function index()
    {
        $results = Service::all();
        return view('admin.service.index',compact('results'));
    }

    public function create()
    {
        return view('admin.service.create');
    }

    public function store(ServiceRequest $request)
    {
        Service::create($request->all());
        return redirect()->route('service.index');
    }

    public function edit(Service $service)
    {
        return view('admin.service.edit',compact('service'));
    }

    public function update(ServiceRequest $request, Service $service)
    {
        $service->update($request->all());
        return redirect()->route('service.index');
    }

    public function destroy(Service $service)
    {
        $service->delete();
        return redirect()->route('service.index');
    }
}
