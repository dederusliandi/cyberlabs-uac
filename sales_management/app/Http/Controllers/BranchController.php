<?php

namespace App\Http\Controllers;

use App\Model\Branch;
use App\Model\AgentType;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;

class BranchController extends Controller
{
    public function index()
    {
        $results = Branch::orderBy('id','DESC')->paginate(10);
        return view('admin.branch.index',compact('results'));
    }

    public function create()
    {
        $agentTypes = AgentType::all();
        return view('admin.branch.create',compact('agentTypes'));
    }

    public function store(BranchRequest $request)
    {
        Branch::create($request->all());
        return redirect()->route('branch.index');
    }

    public function show(Branch $branch)
    {
        return view('admin.branch.show',compact('branch'));
    }

    public function edit(Branch $branch)
    {
        $agentTypes = AgentType::all();
        return view('admin.branch.edit',compact('branch','agentTypes'));
    }

    public function update(BranchRequest $request, Branch $branch)
    {
        $branch->update($request->all());
        return redirect()->route('branch.index');
    }

    public function destroy(Branch $branch)
    {
        $branch->delete();
        return redirect()->route('branch.index');
    }
}
