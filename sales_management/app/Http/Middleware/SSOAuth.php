<?php

namespace App\Http\Middleware;

use Closure;

class SSOAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('token'))
        {
            return redirect('http://127.0.0.1:8000/login?page=http://127.0.0.1:8001/&callback=http://127.0.0.1:8001/callback');
        }
        return $next($request);
    }
}
