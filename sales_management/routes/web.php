<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('callback','LoginController@callback');

// Route::group(['middleware' => 'SSOAuth'],function(){
    Route::get('/','DashboardController@index')->name('dashboard');
    Route::resource('agent-type','AgentTypeController')->except(['show']);
    Route::resource('branch','BranchController');
    Route::resource('service','ServiceController')->except(['show']);
    Route::resource('delivery-transaction','DeliveryTransactionController');

    // item delivery
    Route::group(['prefix' => 'delivery-transaction/{id}'],function(){
        Route::resource('item-delivery','ItemDeliveryController')->only(['create','store']);
    });
// });
