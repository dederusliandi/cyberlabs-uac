@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">

    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header>
                    <h5>outbox detail</h5>
                </header>

                <table class="table table-striped">
                        <tr>
                            <th width="200px">from</th>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th>to</th>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th>subject</th>
                        <td>{{ $result->subject }}</td>
                        </tr>
                        <tr>
                            <th>content</th>
                            <td>{!! $result->content !!}</td>
                        </tr>
                        <tr>
                            <th>file</th>
                            <td>
                                @foreach($result->attachment as $attachment)
                                    <a href="#">{{ $attachment->filename }}</a> <br>
                                @endforeach 
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection