@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">

    <div class="row">
        <div class="col-md-12">
            <div class="admin-content-con clearfix">
                <header>
                    <h5>outbox</h5>
                </header>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>subject</th>
                            <th>content</th>
                            <th>created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($results as $key => $result)
                            <tr>
                                <td>{{ $key+ 1 }}</td>
                                <td>{{ $result->subject }}</td>
                                <td>
                                   {{ strip_tags(strlen($result->content) > 200 ? substr($result->content,0,200).'....' : $result->content) }}
                                </td>
                                <td>{{ $result->created_at->diffForHumans() }}</td>
                                <td><a href="{{ route('message.outboxShow',$result->id) }}" class="label label-success">View</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection