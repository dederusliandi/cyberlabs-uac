@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/chosen_v1.4.0/chosen.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/font-awesome-4.3.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/summernote-master/dist/summernote.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header>
        <h2 class="page_title">Compose</h2>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('message.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label class="">subject</label>
                    <input type="text" name="subject" class="form-control" id="title" placeholder="Subject">
                </div>

                <div class="form-group">
                    <label class="">To</label>
                    <select data-placeholder="To" id="user_id" multiple class="form-control chosen-select" name="user_id[]">
                        <option value="1">dede rusliandi</option>
                        <option value="2">muhammad angga</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="">Message</label>
                    <textarea class="form-control summernote" placeholder="Message" name="content"></textarea>
                </div>

                <div id="file">
                    <div class="form-group">
                        <div class="input-group" >
                            <input required type="file" name="file[]" class="form-control" id="fitur" placeholder="Fitur" value="">

                            <span class="input-group-btn">
                                <button class="btn btn-success btn-add" name="add" id="add" type="button">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save / Publish</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('assets/vendor/chosen_v1.4.0/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/summernote-master/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/default.js') }}"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select' : {},
            '.chosen-select-deselect' : {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-result': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width' : {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>

    <script type="text/javascript">
        $('.summernote').summernote({
            height: 200
        })
    </script>

    <script>
        $(function(){
            var a = 1;

            $(document).on('click','#add',function(){
                a++;
                var html = '<div class="form-group" id="content_'+a+'">';
                    html += '<div class="input-group" >';
                    html += '<input required type="file" name="file[]" class="form-control" id="fitur" placeholder="Fitur" value="">';
                    html += '<span class="input-group-btn">';
                    html += '<button class="btn btn-danger btn-remove" name="add" id="'+a+'" type="button">';
                    html += '<span class="glyphicon glyphicon-minus"></span>';
                    html += '</button>';
                    html += '</span>';
                    html += '</div>';
                    html += '</div>';
                    $('#file').append(html);
            });

            $(document).on('click','.btn-remove',function(){
                var id = $(this).attr("id");
                $('#content_'+a).remove();
                a--;
            });
        });
    </script>
@endsection