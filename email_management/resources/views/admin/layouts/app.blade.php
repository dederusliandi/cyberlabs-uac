<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>

    <!-- Bootstrap -->
    @include('admin.layouts.head')
</head>
<body>

<div class="container-fluid display-table">
    <div class="row display-table-row">
        <!-- side menu -->
       @include('admin.layouts.sidebar')

        {{-- main content area --}}
        <div class="col-md-10 col-sm-11 display-table-cell valign-top">
            <div class="row">
                {{-- header --}}
                @include('admin.layouts.header')
            </div>

            @section('content')
            @show

            @include('admin.layouts.footer')
        </div>
    </div>
</div>

        @include('admin.layouts.script')
    </body>
</html>