<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('assets/vendor/bootstrap-3.3.2-dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/default.js') }}"></script>

@section('script')
@show