<div class="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">
    <br>
    <h1 class="hidden-xs hidden-sm">Email</h1>
    <br>
    <ul>

        <li class="link">
        <a href="{{ route('message.create') }}">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">compose</span>
            </a>
        </li>

        <li class="link">
            <a href="tags.html">
                <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Inbox</span>
            </a>
        </li>

        <li class="link">
        <a href="{{ route('message.outbox') }}">
                <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Outbox</span>
            </a>
        </li>
    </ul>
</div>