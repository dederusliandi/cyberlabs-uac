<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'user_id',
        'subject',
        'content',
    ];

    protected $dates = ['created_at'];

    public function attachment()
    {
        return $this->hasMany('App\Model\Attachment');
    }
}
