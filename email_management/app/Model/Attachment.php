<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
        'message_id',
        'filename'
    ];

    public function message()
    {
        return $this->belongsTo('App\Model\Message');
    }
}
