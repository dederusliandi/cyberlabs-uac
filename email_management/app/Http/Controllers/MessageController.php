<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Message;
use App\Model\Receiver;
use App\Model\Attachment;

class MessageController extends Controller
{
    function create()
    {
        return view('admin.message.create');
    }

    function outbox()
    {
        $results = Message::orderBy('id','DESC')->get();
        return view('admin.outbox.index',compact('results'));
    }

    function outboxShow($id)
    {
        $result = Message::find($id);
        return view('admin.outbox.show',compact('result'));
    }

    function store(Request $request)
    {
        $message = Message::create([
            'user_id'  => 1,
            'subject'  => $request->subject,
            'content'  => $request->content
        ]);

        foreach($request->user_id as $user){
            Receiver::create([
                'user_id'       => $user,
                'message_id'    => $message->id
            ]);
        }

        foreach($request->file as $file){
            $filename = $file->hashName();
            $file->store('public/file');

            Attachment::create([
                'message_id'    => $message->id,
                'filename'      => $filename
            ]);
        }

        return redirect()->route('message.outbox');
    }
}
