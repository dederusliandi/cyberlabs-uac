@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
    <header class="clearfix">
        <h5 class="pull-left">Create role</h5>
        <a class="btn btn-xs btn-primary pull-right" href="{{ route('role.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('role.store') }}" method="POST">
                @csrf
                @if($errors->any())
                    <div class="form-group {{ $errors->has('role') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Role Name</label>
                    <input type="text" name="role" class="form-control" placeholder="Role Name" value="{{ old('role') }}">
                    <span class="help-block">{{ $errors->first('role') }}</span>
                </div>

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection