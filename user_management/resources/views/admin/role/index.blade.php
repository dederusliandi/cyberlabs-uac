@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12 dashboard-left-cell">
            <div class="admin-content-con">
                <header class="clearfix">
                    <h5 class="pull-left">Roles</h5>
                     <a class="btn btn-xs btn-primary pull-right" href="{{ route('role.create') }}" role="button">Create new role</a>
                </header>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $index => $result)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $result->role }}</td>
                            <td>
                                <a class="btn btn-xs btn-warning" href="{{ route('role.edit',$result->id) }}" role="button">edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <nav>
                    {{ $results->links() }}
                </nav>
            </div>

            

        </div>
    </div>            
</div>
@endsection