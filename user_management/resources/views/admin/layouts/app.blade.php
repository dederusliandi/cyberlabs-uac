<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>

    <!-- Bootstrap -->
    @include('admin.layouts.head')
</head>
<body>

<div class="container-fluid display-table">
    <div class="row display-table-row">
        <!-- side menu -->
       @include('admin.layouts.sidebar')

        {{-- main content area --}}
        <div class="col-md-10 col-sm-11 display-table-cell valign-top">
            <div class="row">
                {{-- header --}}
                @include('admin.layouts.header')
            </div>

            @section('content')
            @show

            <div class="row">
                <footer id="admin-footer" class="clearfix">
                    <div class="pull-left"><b>Copyright </b>&copy; 2018</div>
                    <div class="pull-right">admin system</div>
                </footer>
            </div>
        </div>
    </div>
</div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('assets/vendor/bootstrap-3.3.2-dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/default.js') }}"></script>
    </body>
</html>