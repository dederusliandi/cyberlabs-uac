<div class="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">
    <br>
    <h1 class="hidden-xs hidden-sm">Provider</h1>
    <br>
    <ul>
        <li class="link {{ $page == 'user' ? 'active' : '' }}">
            <a href="{{ route('user.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Users</span>
            </a>
        </li>

        <li class="link {{ $page == 'role' ? 'active' : '' }}">
            <a href="{{ route('role.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="hidden-sm hidden-xs">Roles</span>
            </a>
        </li>
    </ul>
</div>