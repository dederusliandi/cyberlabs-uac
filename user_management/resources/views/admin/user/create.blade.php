@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
     <header class="clearfix">
        <h5 class="pull-left">Create user</h5>
        <a class="btn btn-xs btn-primary pull-right" href="{{ route('user.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('user.store') }}" method="POST">
                @csrf
                @if($errors->any())
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Full Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Full Name" value="{{ old('name') }}">
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                    <span class="help-block">{{ $errors->first('email') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" placeholder="Password">
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>

                <div class="form-group">
                    <label>Password Confirmation</label>
                    <input type="text" name="password_confirmation" class="form-control" placeholder="Password Confirmation">
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('role_id') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Role</label>
                    <select name="role_id" id="" class="form-control">
                        <option value="">Role</option>
                        @foreach($roles as $role)
                    <option value="{{ $role->id }}" {{ $role->id == old('role_id') ? 'selected' : '' }}>{{ $role->role }}</option>
                        @endforeach
                    </select>
                    <span class="help-block">{{ $errors->first('role_id') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Status</label>
                    <select name="status" id="" class="form-control">
                        <option value="">Status</option>
                        <option value="1" {{ old('status') == 1 ? 'selected' : '' }}>active</option>
                        <option value="0" {{ old('status') == 0 ? 'selected' : '' }}>inactive</option>
                    </select>
                    <span class="help-block">{{ $errors->first('status') }}</span>
                </div>

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection