@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/new-article.css') }}">
@endsection

@section('content')
<div id="content">
     <header class="clearfix">
        <h5 class="pull-left">Update user</h5>
        <a class="btn btn-xs btn-primary pull-right" href="{{ route('user.index') }}" role="button">Back</a>
    </header>

    <div class="content-inner">
        <div class="form-wrapper">
            <form action="{{ route('user.update',$user->id) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                <input type="hidden" value="{{ $user->id }}" name="id">
                @if($errors->any())
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Full Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Full Name" value="{{ $errors->any() ? old('name') : $user->name }}">
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $errors->any() ? old('email') : $user->email }}">
                    <span class="help-block">{{ $errors->first('email') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('role_id') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Role</label>
                    <select name="role_id" id="" class="form-control">
                        <option value="">Role</option>
                        @foreach($roles as $role)
                    <option value="{{ $role->id }}" {{ $errors->any() ? (old('role_id') == $role->id ? 'selected' : '') : ($role->id == $user->role_id ? 'selected' : '') }}>{{ $role->role }}</option>
                        @endforeach
                    </select>
                    <span class="help-block">{{ $errors->first('role_id') }}</span>
                </div>

                @if($errors->any())
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}">
                @else
                    <div class="form-group">
                @endif
                    <label>Status</label>
                    <select name="status" id="" class="form-control">
                        <option value="">Status</option>
                        <option value="1" {{ $errors->any() ? (old('status') == 1 ? 'selected' : '') : ($user->status == 1 ? 'selected' : '') }}>active</option>
                        <option value="0" {{ $errors->any() ? (old('status') == 0 ? 'selected' : '') : ($user->status == 0 ? 'selected' : '') }}>inactive</option>
                    </select>
                    <span class="help-block">{{ $errors->first('status') }}</span>
                </div>

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection