@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/index.css') }}">
@endsection

@section('content')
<div id="dashboard-con">
    <div class="row">
        <div class="col-md-12 dashboard-left-cell">
            <div class="admin-content-con">
                <header class="clearfix">
                    <h5 class="pull-left">Users</h5>
                <a class="btn btn-xs btn-primary pull-right" href="{{ route('user.create') }}" role="button">Create new user</a>
                </header>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>role</th>
                            <th>status</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{ $result->name }}</td>
                            <td>{{ $result->email }}</td>
                            <td>{{ $result->role->role }}</td>
                            <td>
                                @if($result->status == 1)
                                    <label for="" class="label label-success">active</label>
                                @else
                                    <label for="" class="label label-default">non active</label>
                                @endif
                            </td>
                            <td>
                            <a class="btn btn-xs btn-warning" href="{{ route('user.edit',$result->id) }}" role="button">edit</a>
                            <form action="{{ route('user.destroy',$result->id) }}" method="POST" style="display:inline-block;">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-xs btn-danger">del</button>    
                            </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <nav>
                    {{ $results->links() }}
                </nav>
            </div>

            

        </div>
    </div>            
</div>
@endsection