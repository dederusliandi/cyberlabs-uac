<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/login.css') }}">
</head>
<body>
	<div id="login">
		<div class="content">
			<div class="form-content">
                <form class="form-horizontal" method="POST" action="{{ route('login.store') }}">
                    @csrf
                    <input type="text" name="callback" value="{{ @$callback }}">
                    <input type="text" name="page" value="{{ @$page }}">
                    <img src="{{ asset('assets/img/bkm_logo.png') }}" alt="" width="" class="logo-bkm">
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="email">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Sign in" class="btn btn-primary btn-block">
                    </div>
                </form>
			</div>
		</div>
	</div>
</body>
</html>