<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login','loginController@index')->name('login.form');
Route::post('/login','loginController@login')->name('login.store');
Route::get('api/user','UserController@getUser');

// Route::group(['middleware' => 'SSOAuth'],function(){
    // Route::get('/sudahLogin','loginController@sudahLogin')->name('login.sudahLogin');
    Route::resource('user','UserController');
    Route::resource('role','RoleController')->except(['show','destroy']);
// });
