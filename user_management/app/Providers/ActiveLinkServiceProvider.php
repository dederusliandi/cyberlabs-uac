<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Request;

class ActiveLinkServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $page = '';

        if(Request::segment(1) == 'user')
        {
            $page = 'user';
        }

        if(Request::segment(1) == 'role')
        {
            $page = 'role';
        }

        view()->share('page',$page);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
