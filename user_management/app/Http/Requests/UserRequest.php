<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = \App\User::find($this->users);
        if($this->method() == 'PATCH')
        {
            $data = [
                'name'      => 'required|string|max:191',
                'email'     => 'required|string|email|max:191|unique:users,email,'. $this->get('id'),
                'role_id'   => 'required',
                'status'    => 'required'
            ];
        }
        else
        {
            $data = [
                'name'      => 'required|string|max:191',
                'email'     => 'required|string|email|max:191|unique:users',
                'password'  => 'required|string|min:6|confirmed',
                'role_id'   => 'required',
                'status'    => 'required'
            ];
        }
        return $data;
    }
}
