<?php

namespace App\Http\Controllers;

use App\Model\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{

    public function index()
    {
        $results = Role::paginate(10);
        return view('admin.role.index',compact('results'));
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(RoleRequest $request)
    {
        Role::create($request->all());
        return redirect()->route('role.index');
    }

    public function edit(Role $role)
    {
        return view('admin.role.edit',compact('role'));
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->all());
        return redirect()->route('role.index');
    }

}
