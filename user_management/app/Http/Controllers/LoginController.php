<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;

class LoginController extends Controller
{
    function index(Request $request)
    {
        $callback   = $request->callback;
        $page       = $request->page;

        return view('admin.login.login',compact('callback','page'));
    }

    function login(Request $request)
    {
        $callback   = $request->callback; //127.0.0.1/callback client 1
        $page       = $request->page;
        $email      = $request->email;
        $token      = str_random(100);

        $user = User::where('email',$email)->first();
        $user->update([
            "token" => $token
        ]);

        Session::put('token',$token);

        if(!$callback && !$page) {
            return redirect()->route('user.index');
        }
        return redirect()->intended($callback.'?token='.$token.'&page?='.$page);
    }

    function sudahLogin()
    {
        Session::flush();
    }


}
