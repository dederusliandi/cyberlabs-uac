<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;
use App\Model\Role;

class UserController extends Controller
{
    public function index()
    {
        $results = User::orderBy('id','DESC')->paginate(10);
        return view('admin.user.index',compact('results'));
    }

    public function create()
    {
        $roles   = Role::all();
        return view('admin.user.create',compact('roles'));
    }

    public function store(UserRequest $request)
    {

        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        
        User::create($input);
        return redirect()->route('user.index');
    }

    public function show(User $user)
    {
        return view('admin.layout.show',compact('user'));
    }

    public function edit(User $user)
    {
        $roles   = Role::all();
        return view('admin.user.edit',compact('roles','user'));
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('user.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('user.index');
    }

    public function getUser()
    {
        return User::all();
    }
}
