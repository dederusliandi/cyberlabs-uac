<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class SSOAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('token') == FALSE)
        {
            return redirect()->route('login.form');
        }
        
        return $next($request);
    }
}
