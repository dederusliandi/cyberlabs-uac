<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role'];

    function user()
    {
        return $this->hasOne('App\User');
    }
}
