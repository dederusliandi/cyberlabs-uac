<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            [
                'name'          => 'dede rusliandi',
                'email'         => 'dede.rusliandi1@gmail.com',
                'password'      => bcrypt('dede.rusliandi1@gmail.com'),
                'role_id'       => 1,
                'status'        => 1,
                'token'         => str_random(100)
            ],
            [
                'name'          => 'muhammad angga',
                'email'         => 'muhammad.angga@gmail.com',
                'password'      => bcrypt('muhammad.angga@gmail.com'),
                'role_id'       => 1,
                'status'        => 1,
                'token'         => str_random(100)
            ]
        ]);
    }
}
