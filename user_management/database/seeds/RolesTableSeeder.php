<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Role::insert([
            [
                'id'    => 1,
                'role'  => 'admin',
            ],
            [
                'id'    => 2,
                'role'  => 'sales',
            ],
            [
                'id'    => 3,
                'role'  => 'customer'
            ]
        ]);
    }
}
